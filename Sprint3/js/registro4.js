let registros = [];

function recibirEdad(){
    let edad;
    //edad = prompt("Digite su edad");
    return edad;    
}

function ordenarEdad(edad){         
    if (registros.length < 20) { 
        let datosDegistro = {
            'nombre': document.getElementById("campoNombres"),
            'contrasena': document.getElementById("campoContraseña"),
            'correo': document.getElementById("campoEmail"),
            'confirmacionContrasena': document.getElementById("campoConfirmar"),
            'telefono': document.getElementById("campoTelefono"),
            'edad': edad
        };
        registros.push(datosDegistro); 
    } else {
        alert("Tamaño máximo del arreglo alcanzado")
    }

    registros.sort((a, b) => b.edad - a.edad);   
    return registros; 
}

function promedioEdad(registros){ 
    let suma = 0;       
    registros.forEach(registro => {
        suma += Number(registro.edad);
    });
    
    let promedio = suma/registros.length;    
    return promedio;          
}

module.exports.recibirEdad = recibirEdad;
module.exports.ordenarEdad = ordenarEdad;
module.exports.promedioEdad = promedioEdad;