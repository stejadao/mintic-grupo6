function validar_nombre(nombre){
    //const inputName = document.getElementById("campoNombre");
    let nombre_validado = false;
    let formato_valido = /^[A-Za-z\s]+$/;

    if (typeof nombre === 'string' && 4 < nombre.length <= 30 && formato_valido.test(nombre)) {
        nombre_validado = true; 
    } 
 
    return nombre_validado;
}

function validar_contrasenas(contrasena){
    //const inputContrasena = document.getElementById("Password");
    //const inputConfirmacion = document.getElementById("campoConfirmarContrasena");

    let validacion = true;
    let confirmacion = document.getElementById("campoConfirmarContrasena");

    if(typeof(confirmacion) == 'undefined' || confirmacion == null || contrasena.length != confirmacion.innerHTML.length || contrasena != confirmacion.innerHTML) {
        validacion = false;
    }

    return validacion;

}

module.exports.validar_nombre = validar_nombre;
module.exports.validar_contrasenas = validar_contrasenas;


