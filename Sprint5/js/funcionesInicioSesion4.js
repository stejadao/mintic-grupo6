let registros = [];

function ordenarEdad(edad){         
    if (registros.length < 20) { 
        let datosDegistro = {
            'nombre': document.getElementById("campoNombres"),
            'contrasena': document.getElementById("campoContraseña"),
            'correo': document.getElementById("campoEmail"),
            'confirmacionContrasena': document.getElementById("campoConfirmar"),
            'telefono': document.getElementById("campoTelefono"),
            'edad': edad
        };
        registros.push(datosDegistro); 
    } else {
        alert("Tamaño máximo del arreglo alcanzado")
    }

    registros.sort((a, b) => b.edad - a.edad);   
    return registros; 
}

function verificarInicioSesion4(correo, contrasena, arreglo) {
    let correos = arreglo.map(a => a.correo);
    let contrasenas = arreglo.map(a => a.contrasena);

    if (correos.includes(correo) && contrasenas.includes(contrasena)) {
        let respuesta;
        //respuesta = prompt("¿En qué año se dio la batalla de Boyacá?");
        return verificarCaptcha(respuesta);          
    } else {
        return false;
    }
}

function verificarCaptcha(respuesta) {
    if (respuesta == '1819') {
        return true;
    } else {
        return false;
    }
}

module.exports.ordenarEdad = ordenarEdad;
module.exports.verificarInicioSesion4 = verificarInicioSesion4;
module.exports.verificarCaptcha = verificarCaptcha;
